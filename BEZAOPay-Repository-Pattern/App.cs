﻿using System;
using System.Text;
using BEZAOPay_Repository_Pattern.Applications;

namespace BEZAOPay_Repository_Pattern
{
    public static class App
    {
        static private StringBuilder StringBuilder = new StringBuilder();
        private static UserApplication _userApplication = new UserApplication();
        private static OperationApplication _opApplication = new OperationApplication();

        public static void Run()
        {
            mainmenu:
                StringBuilder.Clear();
                StringBuilder.AppendLine("Welcome to the Generic Management System");
                StringBuilder.AppendLine("Press:");
                StringBuilder.AppendLine("1. User Management");
                StringBuilder.AppendLine("2. User Operations");
                StringBuilder.AppendLine("3. Exit");

            var running = true;

            while (running)
            {
                Console.WriteLine(StringBuilder.ToString());
                switch (Console.ReadLine())
                {
                    case "1":
                        _userApplication.Run();
                        break;

                    case "2":
                        _opApplication.Run();
                        break;

                    case "3":
                        running = false;
                        Console.WriteLine("\nThanks and GoodBye!...\nRun the App to use again!...\n");
                        break;
                    default:
                        Console.WriteLine("\nInvalid input...\nTry Again!!\n");
                        goto mainmenu;
                }
            }
        }

        private static void DisplayPrompt()
        {
            Console.WriteLine("\nDo you want to perform another operation? \n1. Yes\n2. Any other Key to exit!");
        }
    }
}
