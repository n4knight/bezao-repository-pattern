﻿using System;
using System.Text;
using DataAccessLayer.Interfaces.Services;
using DataAccessLayer.Models;
using DataAccessLayer.UnitOfWork;
using DataAccessLayer.Entities;
using DataAccessLayer.Services;

namespace BEZAOPay_Repository_Pattern.Applications
{
    public class OperationApplication
    {
        private StringBuilder stringBuilder = new StringBuilder();

        public void Run()
        {
            MainMenu:
            stringBuilder.Clear();
            stringBuilder.AppendLine("Press");
            stringBuilder.AppendLine("1. Deposit");
            stringBuilder.AppendLine("2. Withdraw");
            stringBuilder.AppendLine("3. Transfer");
            stringBuilder.AppendLine("4. Get Account Details");
            stringBuilder.AppendLine("5. MainMenu");


            var running = true;
            while (running)
            {
                Console.WriteLine(stringBuilder.ToString());
                switch (Console.ReadLine())
                {
                    case "1":
                        Deposit();
                        break;
                    case "2":
                        Withdraw();
                        break;
                    case "3":
                        Transfer();
                        break;
                    case "4":
                        Console.Write("Enter ID of User to Find ");
                        var id = Console.ReadLine();
                        GetAccount(checkInput(id));
                        break;
                    case "5":
                        App.Run();
                        running = false;
                        break;
                    default:
                        Console.WriteLine("Invalid Input");
                        goto MainMenu;
                }
            }
        }

        void Deposit()
        {
            Console.WriteLine("To Deposit Money enter the following\n");

            Console.Write("Enter your account Number  ");
            var accountNumber = Console.ReadLine();
            Console.Write("Enter the amount to Deposit  ");

            var amount = Console.ReadLine();

            try
            {
                ITransactionService transactionService = new TransactionService(new UnitOfWork(new BEZAOContext()));
                transactionService.Deposit(new DepositViewModel
                {
                    AccountNumber = Helpers.Helpers.validateInt(accountNumber),
                    Amount = Helpers.Helpers.validateDecimal(amount)
                });
                Console.WriteLine();

            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        void Withdraw()
        {
            Console.WriteLine("To Withdraw Money enter the following\n");
            Console.Write("Enter your account Number  ");

            var accountNumber = Console.ReadLine();
            Console.Write("Enter the amount to Withdraw  ");

            var amount = Console.ReadLine();
            try
            {
                ITransactionService transactionService = new TransactionService(new UnitOfWork(new BEZAOContext()));
                transactionService.Withdraw(new WithdrawViewModel
                {
                    AccountNumber = Helpers.Helpers.validateInt(accountNumber),
                    Amount = Helpers.Helpers.validateDecimal(amount)
                });
                Console.WriteLine();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        void Transfer()
        {
            Console.WriteLine("To Transfer Money enter the following\n");

            Console.Write("Enter your account Number  ");
            var accountNumber = Console.ReadLine();
            Console.Write("Enter receivers account Number  ");
            var rAccountNumber = Console.ReadLine();

            Console.Write("Enter the amount to Transfer  ");
            var amount = Console.ReadLine();

            try
            {
                ITransactionService transactionService = new TransactionService(new UnitOfWork(new BEZAOContext()));
                transactionService.Transfer(new TransferViewModel
                {
                    SenderAccountNumber = Helpers.Helpers.validateInt(accountNumber),
                    ReceiverAccountNumber = Helpers.Helpers.validateInt(rAccountNumber),
                    Amount = Helpers.Helpers.validateDecimal(amount)
                });
                Console.WriteLine();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        void GetAccount(int id)
        {
            try
            {
                IAccountService accountService = new AccountService(new UnitOfWork(new BEZAOContext()));
                accountService.GetAccount(id);
                Console.WriteLine();

            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void DisplayPrompt()
        {
            Console.WriteLine("Go to User Menu ?\n1. Yes\n2. Any other Key to go back main menu!");
        }

        private int checkInput(string id)
        {

            while (string.IsNullOrWhiteSpace(id))
            {
                Console.WriteLine("ID cannot be a string ");
                id = Console.ReadLine();
            }

            return int.Parse(id);
        }

    }
}
