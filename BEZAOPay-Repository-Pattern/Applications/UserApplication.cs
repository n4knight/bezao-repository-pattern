﻿using System;
using System.Text;
using DataAccessLayer.Interfaces.Services;
using DataAccessLayer.Models;
using DataAccessLayer.UnitOfWork;
using DataAccessLayer.Entities;
using DataAccessLayer.Services;

namespace BEZAOPay_Repository_Pattern.Applications
{
    public class UserApplication
    {
        private StringBuilder stringBuilder = new StringBuilder();
        private OperationApplication _opApplication = new OperationApplication();

        public void Run()
        {
            MainMenu:
            stringBuilder.Clear();
            stringBuilder.AppendLine("Press");
            stringBuilder.AppendLine("1. Register");
            stringBuilder.AppendLine("2. Login");
            stringBuilder.AppendLine("3. Update");
            stringBuilder.AppendLine("4. Delete");
            stringBuilder.AppendLine("5. Get");
            stringBuilder.AppendLine("6. MainMenu");


            var running = true;
            
            while (running)
            {
                Console.WriteLine(stringBuilder.ToString());
                switch (Console.ReadLine())
                {
                    case "1":
                        Register();
                        break;
                    case "2":
                        Login();
                        _opApplication.Run();
                        break;
                    case "3":
                        Update();
                        break;
                    case "4":
                        Console.Write("Enter ID of User to delete ");
                        var deleteId = Console.ReadLine();
                        Delete(checkInput(deleteId));
                        break;
                    case "5":
                        GetAll();
                        break;
                    case "6":
                        App.Run();
                        running = false;
                        break;
                    default:
                        goto MainMenu;

                }
            }
        }

        private int checkInput(string id)
        {

            while (string.IsNullOrWhiteSpace(id))
            {
                Console.Write("ID cannot be a string ");
                id = Console.ReadLine();
            }

            return int.Parse(id);
        }

        void Register()
        {
            IUserService userService = new UserService(new UnitOfWork(new BEZAOContext()));
            try
            {
                userService.Register(Helpers.Helpers.EnterUserDetails());
                Console.WriteLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        void Login()
        {
            IUserService userService = new UserService(new UnitOfWork(new BEZAOContext()));

            Console.Write("Enter Email or Username to Log in:  ");
            var userNameEmail = Console.ReadLine();
            while (Helpers.Helpers.isNumber(userNameEmail) || !Helpers.Helpers.isEmail(userNameEmail))
            {
                Console.Write("Your Email or Username will login you in:  ");
                userNameEmail = Console.ReadLine();
            }

            Console.Write("Enter password:  ");
            var password = Console.ReadLine();
            while (Helpers.Helpers.isNumber(password))
            {
                Console.Write("incorrect password, try again:  ");
                password = Console.ReadLine();
            }


            try
            {
                userService.Login(new LoginViewModel { UsernameEmail = userNameEmail, Password = password });
                Console.WriteLine();

            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        void Update()
        {
            Console.WriteLine("Be advised, You can only update your Password!");
            try
            {
                IUserService userService = new UserService(new UnitOfWork(new BEZAOContext()));
                userService.Update(Helpers.Helpers.UpdateUser());
                Console.WriteLine();
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine();
        }

        void Delete(int id)
        {
            try
            {
                IUserService userService = new UserService(new UnitOfWork(new BEZAOContext()));
                userService.Delete(id);
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine();
        }

        void GetAll()
        {
            try
            {
                IUserService userService = new UserService(new UnitOfWork(new BEZAOContext()));
                userService.GetAll();
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine();
        }

        private void DisplayPrompt()
        {
            Console.WriteLine("Go to User Menu ?\n1. Yes\n2. Any other Key to go back main menu!");
        }
    }
}
