﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using DataAccessLayer.Models;

namespace BEZAOPay_Repository_Pattern.Helpers
{
    public static class Helpers
    {
        public static RegisterViewModel EnterUserDetails()
        {
            Console.Write("Enter your FirstName  ");
            var firstname = Console.ReadLine();
            while (isNumber(firstname))
            {
                Console.Write("FirstName is required  ");
                firstname = Console.ReadLine();
            }

            Console.Write("Enter your LastName  ");
            var lastname = Console.ReadLine();
            while (isNumber(lastname))
            {
                Console.Write("LastName is required  ");
                lastname = Console.ReadLine();
            }

            Console.Write("Enter your Email  ");
            var email = Console.ReadLine();
            while (!isEmail(email))
            {
                Console.Write("Email is required  ");
                email = Console.ReadLine();
            }

            Console.Write("Enter your Username ");
            var username = Console.ReadLine();
            while (isNumber(username))
            {
                Console.Write("Username is required  ");
                username = Console.ReadLine();
            }

            Console.Write("Enter your Birthday (mm/dd/yy)  ");
            var birthday = Console.ReadLine();
            while (!isValidDate(birthday))
            {
                Console.Write("Birthday is required (mm/dd/yy)  ");
                username = Console.ReadLine();
            }

            Console.Write("Enter your Password  ");
            var password = Console.ReadLine();
            while (isNumber(password))
            {
                Console.Write("Password is required  ");
                password = Console.ReadLine();
            }

            Console.Write("Confirm your password  ");
            var confirmPassword = Console.ReadLine();
            while (isNumber(confirmPassword))
            {
                Console.Write("Confirm password again  ");
                confirmPassword = Console.ReadLine();
            }

            return new RegisterViewModel
            {
                FirstName = firstname,
                LastName = lastname,
                Birthday = parseDate(birthday),
                Username = username,
                Password = password,
                ConfirmPassword = confirmPassword,
                Email = email,
            };
        }
        public static UpdateViewModel UpdateUser()
        {
            Console.Write("Enter your Username ");
            var username = Console.ReadLine();
            while (isNumber(username))
            {
                Console.Write("Username is required  ");
                username = Console.ReadLine();
            }

            Console.Write("Enter your Email  ");
            var email = Console.ReadLine();
            while (!isEmail(email))
            {
                Console.Write("Email is required  ");
                email = Console.ReadLine();
            }

            Console.Write("Enter your CurrentPassword  ");
            var password = Console.ReadLine();
            while (isNumber(password))
            {
                Console.Write("Password is required  ");
                password = Console.ReadLine();
            }

            Console.Write("Enter your New Password  ");
            var NewPassword = Console.ReadLine();
            while (isNumber(NewPassword))
            {
                Console.Write("New Password is required  ");
                password = Console.ReadLine();
            }

            Console.Write("Confirm your New Password  ");
            var confirmNewPassword = Console.ReadLine();
            while (isNumber(confirmNewPassword))
            {
                Console.Write("Confirm new Password  ");
                password = Console.ReadLine();
            }

            return new UpdateViewModel
            {
                Username = username,
                Email = email,
                CurrentPassword = password,
                NewPassword = NewPassword,
                ConfirmNewPassword = confirmNewPassword
            };
        }

        public static bool isNumber(string input)
        {
            return int.TryParse(input, out _) || string.IsNullOrWhiteSpace(input) || input.Length < 3;
        }

        public static decimal validateDecimal (string input)
        {
            while (string.IsNullOrWhiteSpace(input))
            {
                Console.Write("Must be a Number  ");
                input = Console.ReadLine();
            }

            while(decimal.Parse(input) <= 0)
            {
                Console.Write("Invalid amount. Try again ");
                input = Console.ReadLine();
            }
            return decimal.Parse(input);
        }

        public static int validateInt(string input)
        {
            while (string.IsNullOrWhiteSpace(input))
            {
                Console.WriteLine("Must be a number");
                input = Console.ReadLine();
            }

            return int.Parse(input);
        }

        public static bool isEmail(string input)
        {
            string emailPattern = @"^((\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)\s*[;,.]{0,1}\s*)+$";
            Regex regex = new Regex(emailPattern);
            Match match = regex.Match(input);

            if (match.Success) return true;

            return false;
        }

        public static bool isValidDate(string input)
        {
            return DateTime.TryParse(input, out _);
        }

        static DateTime parseDate(string input)
        {
            return DateTime.Parse(input);
        }

        static string GetSalt()
        {
            var salt = new Random().Next();
            return salt.ToString();
        }
    }
}
