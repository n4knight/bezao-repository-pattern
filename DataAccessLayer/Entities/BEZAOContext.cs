﻿using Microsoft.EntityFrameworkCore;
using DataAccessLayer.DataInitializer;


namespace DataAccessLayer.Entities
{
    public class BEZAOContext : DbContext
    {

        const string connectionString = @"server=(localdb)\mssqllocaldb; 
                database=CFBEZAO; trusted_Connection=true";

        public BEZAOContext()
        {

        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating (ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("BZ");
            modelBuilder.Seed();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Account> Accounts { get; set; }
    }
}
