﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces.Repositories
{
    public interface IRepository<TNTT> where TNTT : class
    {
        TNTT Get(int id);

        void Add(TNTT entity);

        IQueryable<TNTT> Find(Expression<Func<TNTT, bool>> predicate);

        IQueryable<TNTT> GetAll();

        Task<TNTT> GetAsync(int id);

        Task AddAsync(TNTT entity);
        Task AddRangeAsync(IList<TNTT> entities);

        void Update(TNTT entity);

        void Delete(TNTT entity);
        void DeleteRange(IList<TNTT> entities);



    }
}
