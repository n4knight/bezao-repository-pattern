﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Models;

namespace DataAccessLayer.Interfaces.Services
{
    public interface ITransactionService
    {
        void Deposit(DepositViewModel model);
        void Transfer(TransferViewModel model);
        void Withdraw(WithdrawViewModel model);
    }
}
