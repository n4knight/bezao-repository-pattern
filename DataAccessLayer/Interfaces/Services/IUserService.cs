﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Models;

namespace DataAccessLayer.Interfaces.Services
{
    public interface IUserService
    {
        void Register(RegisterViewModel model);
        void Update(UpdateViewModel model);
        void Login(LoginViewModel model);
        void Delete(int id);
        void Get(int id);

        void GetAll();
    }
}
