﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Interfaces.Repositories;
using DataAccessLayer.Entities;

namespace DataAccessLayer.Interfaces.UnitOfWork
{
    
    public interface IUnitOfWork
    {
        IRepository<Transaction> Transactions { get; }
        IRepository<Account> Accounts { get; }
        IRepository<User> Users { get; }

        int Commit();
        Task<int> CommitAsync();
 
    }
}
