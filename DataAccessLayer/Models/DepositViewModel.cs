﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    public class DepositViewModel
    {
        public int AccountNumber { get; set; }
        public decimal Amount { get; set; }
    }
}
