﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    public class TransferViewModel
    {
        public int ReceiverAccountNumber { get; set; }
        public int SenderAccountNumber { get; set; }
        public decimal Amount { get; set; }
    }
}
