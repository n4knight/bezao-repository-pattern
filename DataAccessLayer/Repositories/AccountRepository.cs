﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Repositories
{
    class AccountRepository: Repository<Account>, IAccountRepository
    {
        private readonly DbContext _context;

        public AccountRepository(BEZAOContext context):
            base(context)
        {
            _context = context;
        }
    }
}
