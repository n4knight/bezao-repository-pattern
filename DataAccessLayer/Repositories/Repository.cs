﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Repositories
{
    class Repository<TNTT>:IRepository<TNTT> where TNTT : class
    {
        private readonly DbContext _context;
        private DbSet<TNTT> _entity;
        public Repository(DbContext context)
        {
            _context = context;
            this._entity = context.Set<TNTT>();
        }

        public IQueryable<TNTT> GetAll()
        {
            return _entity.AsQueryable();
        }


        public TNTT Get(int id)
        {
            return _entity.Find(id);
        }

        public async Task<TNTT> GetAsync(int id)
        {
            return await _entity.FindAsync(id);
        }

        public IQueryable<TNTT> Find(Expression<Func<TNTT, bool>> predicate)
        {
            return _entity.Where(predicate).AsQueryable();
        }

        public void Add(TNTT entity)
        {
            _entity.Add(entity);
        }

        public async Task AddAsync(TNTT entity)
        {
            await _entity.AddAsync(entity);
        }

        public void AddRange(IList<TNTT> entities)
        {
            _entity.AddRange(entities);
        }

        public async Task AddRangeAsync(IList<TNTT> entities)
        {
            await _entity.AddRangeAsync(entities);
        }

        public void Update(TNTT entity)
        {
            _context.Set<TNTT>().Update(entity);
        }

        public void Delete(TNTT entity)
        {
            _entity.Remove(entity);
        }

        public void DeleteRange(IList<TNTT> entities)
        {
            _entity.RemoveRange(entities);
        }

        public TNTT FindByType(Expression<Func<TNTT, bool>> predicate)
        {
            return (TNTT)_entity.Where(predicate);
        }


        public void DeleteRangeAsync(IList<TNTT> entities)
        {
            _entity.RemoveRange(entities);
        }

    }
}
