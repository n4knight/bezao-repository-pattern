﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;


namespace DataAccessLayer.Repositories
{
    class TransactionRepository:Repository<Transaction>, ITransactionRepository
    {
        private readonly DbContext _context;
        public TransactionRepository(BEZAOContext context):
            base(context)
        {
            _context = context;
        }
    }
}
