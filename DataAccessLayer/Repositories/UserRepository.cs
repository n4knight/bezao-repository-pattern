﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces.Repositories;

namespace DataAccessLayer.Repositories
{
    class UserRepository:Repository<User>, IUserRepository
    {
        private readonly DbContext _context;

        public UserRepository(DbContext context) :
            base(context)
        {
            _context = context;

        }
    }
}
