﻿using System;
using DataAccessLayer.Interfaces.Services;
using DataAccessLayer.Interfaces.UnitOfWork;
using DataAccessLayer.Entities;

namespace DataAccessLayer.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWork _unitOfWork;
        public AccountService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void GetAccount(int id)
        {
            Account account = _unitOfWork.Accounts.Get(id);
            if (account is null) throw new Exception($"No account with id {id} found");
            Console.WriteLine($"Account Details with id {id}\n AccountNumber --> {account.AccountNumber}\n" +
                $"Balance --> {account.Balance}");
            Console.WriteLine();
        }
    }
}
