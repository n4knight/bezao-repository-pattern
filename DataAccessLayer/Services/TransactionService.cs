﻿using DataAccessLayer.Interfaces.Services;
using DataAccessLayer.Interfaces.UnitOfWork;
using DataAccessLayer.Models;
using System;
using System.Linq;
using DataAccessLayer.Entities;
using System.Threading.Tasks;

namespace DataAccessLayer.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly IUnitOfWork _unitOfWork;
        public TransactionService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Deposit(DepositViewModel model)
        {
            var user = _unitOfWork.Users.GetAll().
                Where(u => u.Account.AccountNumber == model.AccountNumber).FirstOrDefault();

            if (user is null) throw new Exception($"User with accountNumber: {model.AccountNumber} not found");

            var account = _unitOfWork.Accounts.Get(user.AccountId);
            if (account is null) throw new Exception($"No Associated account found ");
            account.Balance += model.Amount;

            _unitOfWork.Transactions.Add(new Transaction
            {
                Amount = model.Amount,
                TimeStamp = DateTime.Now,
                TransactionMode = TransactionMode.Credit,
                UserId = user.Id
            
            });

            _unitOfWork.Commit();
            Console.WriteLine("Deposit Successful");
            Console.WriteLine();
        }

        public void Transfer(TransferViewModel model)
        {
            var sender = _unitOfWork.Users.GetAll().
                Where(u => u.Account.AccountNumber == model.SenderAccountNumber).FirstOrDefault();
            var receiver = _unitOfWork.Users.GetAll().
                Where(u => u.Account.AccountNumber == model.ReceiverAccountNumber).FirstOrDefault();

            if (sender is null)
                throw new Exception($"No account with number {model.SenderAccountNumber}");
            if (receiver is null)
                throw new Exception($"No account with number {model.ReceiverAccountNumber}");

            var senderAccount = _unitOfWork.Accounts.Get(sender.AccountId);
            var recieverAccount = _unitOfWork.Accounts.Get(receiver.AccountId);

            if (model.Amount > senderAccount.Balance)
            {
                throw new Exception("Insuffiecient funds");
            }

            senderAccount.Balance -= model.Amount;
            recieverAccount.Balance += model.Amount;

            _unitOfWork.Transactions.Add(new Transaction
            {
                Amount = model.Amount,
                TimeStamp = DateTime.Now,
                TransactionMode = TransactionMode.Debit,
                UserId = sender.Id
            });

             _unitOfWork.Transactions.Add(new Transaction
            {
                Amount = model.Amount,
                TimeStamp = DateTime.Now,
                TransactionMode = TransactionMode.Credit,
                UserId = receiver.Id
            });

             _unitOfWork.CommitAsync();
            Console.WriteLine("Transfer completed");

        }

        public void Withdraw(WithdrawViewModel model)
        {
            var user = _unitOfWork.Users.GetAll().
                Where(u => u.Account.AccountNumber == model.AccountNumber).FirstOrDefault();

            if (user is null) 
                throw new Exception($"User with accountNumber: {model.AccountNumber} not found");

            var account = _unitOfWork.Accounts.Get(user.AccountId);
            if (account is null) throw new Exception($"No Associated account found ");

            if (account.Balance < model.Amount) throw new Exception("Insufficient Balance");
            account.Balance -= model.Amount;

            _unitOfWork.Transactions.Add(new Transaction
            {
                Amount = model.Amount,
                TimeStamp = DateTime.Now,
                TransactionMode = TransactionMode.Debit,
                UserId = user.Id

            });

            _unitOfWork.Commit();
            Console.WriteLine("Withdraw Successful");
        }
    }
}
