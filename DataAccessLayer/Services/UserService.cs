﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces.Services;
using DataAccessLayer.Interfaces.UnitOfWork;
using DataAccessLayer.Models;

namespace DataAccessLayer.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void GetAll()
        {
            foreach (var user in _unitOfWork.Users.GetAll())
            {
                Console.WriteLine($"ID: {user.Id}, username: {user.Username}, Email: {user.Email}");
            }
        }
        public void Register(RegisterViewModel model)
        {


            if (!Validate(model))
            {
                return;
            }

            

            var user = new User
            {
                Name = $"{model.FirstName} {model.LastName}",
                Email = model.Email,
                Username = model.Username,
                Birthday = model.Birthday,
                IsActive = true,
                Password = HashPassword(model.ConfirmPassword),
                Account = new Account { AccountNumber = GenerateAccountNumber() },
                Created = DateTime.Now
            };
            _unitOfWork.Users.Add(user);
            _unitOfWork.Commit();
            Console.WriteLine("Success!");
            Console.WriteLine($"Your account number: {user.Account.AccountNumber}");
            Console.WriteLine();
        }

        public void Update(UpdateViewModel model)
        {
            var user = _unitOfWork.Users.Find(u => u.Username == model.Username).FirstOrDefault();
            if (user is null) throw new Exception("No user found");
            user.Password = model.ConfirmNewPassword;
            _unitOfWork.Commit();
            Console.WriteLine("Update done");

        }

        public void Login(LoginViewModel model)
        {
            var user = _unitOfWork.Users.Find(u => u.Username == model.UsernameEmail || 
                u.Email == model.UsernameEmail 
                && u.Password == HashPassword(model.Password)).FirstOrDefault();
            if(user.Password != HashPassword(model.Password)) throw new Exception("User not found");
            Console.WriteLine($"Login successful");
            Console.WriteLine($"{user.Name} welcome");

        }

        public void Delete(int id)
        {
            var user = _unitOfWork.Users.Get(id);
            if (user is null) throw new Exception($"No user with id {id}");
            _unitOfWork.Users.Delete(user);
            _unitOfWork.Commit();
            Console.WriteLine("User Deleted");
        }

        public void Get(int id)
        {
            User user = _unitOfWork.Users.Get(id);
            if (user is null) throw new Exception($"No user with id {id}");
            Console.WriteLine($"User with id {id} -> {user.Name}");
        }

        private bool Validate(RegisterViewModel model)
        {
            //string.IsNullOrWhiteSpace(model.Email) ? ErrorMessage("EMAIL") :
            //    string.IsNullOrWhiteSpace(model.FirstName) ? ErrorMessage("FIRSTNAME") :
            //    string.IsNullOrWhiteSpace(model.LastName) ? ErrorMessage("LASTNAME") :
            //    string.IsNullOrWhiteSpace(model.Username) ? ErrorMessage("USERNAME") :
            //    model.Birthday == new DateTime() ? ErrorMessage("BIRTHDAY") :
            //    string.IsNullOrWhiteSpace(model.Password) ? ErrorMessage("PASSWORD") :
            //    string.IsNullOrWhiteSpace(model.ConfirmPassword) ? ErrorMessage("CONFIRMPASSWORD")

            if (string.IsNullOrWhiteSpace(model.Email))
            {
                Console.WriteLine("Email field is required");
                return false;

            }

            if (string.IsNullOrWhiteSpace(model.FirstName))
            {
                Console.WriteLine("Firstname field is required");
                return false;

            }


            if (string.IsNullOrWhiteSpace(model.LastName))
            {
                Console.WriteLine("Lastname field is required");
                return false;

            }

            if (string.IsNullOrWhiteSpace(model.Username))
            {
                Console.WriteLine("Username field is required");
                return false;

            }
            if (model.Birthday == new DateTime())
            {
                Console.WriteLine("Birthday field is required");

            }
            if (string.IsNullOrWhiteSpace(model.Password))
            {
                Console.WriteLine("Password field is required");
                return false;

            }
            if (string.IsNullOrWhiteSpace(model.ConfirmPassword))
            {
                Console.WriteLine("confirmPassword field is required");
                return false;

            }

            return true;

        }

        private void ErrorMessage(string field)
        {
            Console.WriteLine($"{field} is required");
            return;
        }

        static string HashPassword(string password)
        {
            using (var sha256 = SHA256.Create())
            {
                var hashed = sha256.ComputeHash(Encoding.UTF8.GetBytes(password));
                return BitConverter.ToString(hashed).Replace("-", "").ToLower();
            }
        }

        int GenerateAccountNumber()
        {
            int _loopLimit = 10, counter = 0, range = 4;
            string acc = "";

            while (counter < _loopLimit)
            {
                acc += new Random().Next(range).ToString();
                counter++;
            }

            return int.Parse(acc);
        }
    }
}
