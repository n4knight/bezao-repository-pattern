﻿using System;
using DataAccessLayer.Interfaces.UnitOfWork;
using DataAccessLayer.Interfaces.Repositories;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories;
using System.Threading.Tasks;

namespace DataAccessLayer.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BEZAOContext _context;

        private IRepository<Transaction> _transactions;
        private IRepository<Account> _accounts;
        private IRepository<User> _users;

        public UnitOfWork(BEZAOContext context)
        {
            _context = context;
        }

        public IRepository<User> Users { get { return (_users ?? new UserRepository(_context)); } }
        public IRepository<Transaction> Transactions { get { return _transactions ?? new TransactionRepository(_context); } }
        public IRepository<Account> Accounts { get { return _accounts ?? new AccountRepository(_context); } }

        public int Commit()
        {
            return _context.SaveChanges();
        }


        public async Task<int> CommitAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
